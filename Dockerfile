FROM wordpress
RUN apt update 
RUN apt install unzip wget -y
RUN cd /var/www/html/wp-content/themes && wget https://github.com/bootscore/bootscore/releases/latest/download/bootscore-main.zip
RUN cd /var/www/html/wp-content/themes && unzip bootscore-main.zip